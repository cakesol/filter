<?php

namespace Cakesol\Filter;

use Cakesol\Filter\Form\FilterForm;
use Cake\ORM\Query;

class FilterCollection
{
    /**
     * @var array $data;
     */
    private $data = [];

    /**
     * @var FilterForm;
     */
    private $form;

    /**
     * Collection of the filters which needs to be applied
     * @var FilterInterface[] $filters
     */
    protected $filters = [];

    public function __construct(array $filters = [])
    {
        $this->addFilters($filters);
    }

    /**
     * @param FilterInterface[] $filters
     * @return FilterCollection
     */
    public function addFilters(array $filters = []): self
    {
        $this->filters = array_merge($this->filters, $filters);
        return $this;
    }

    /**
     * Check if there are filters available
     *
     * @return bool
     */
    public function hasFilters(): bool
    {
        return (count($this->filters) > 0);
    }

    /**
     * Validate if the supplied data matches the filters
     *
     * @return bool
     */
    public function validate(array $data): bool
    {
        $this->data = $data;
        return $this->getForm()->validate($data);
    }

    /**
     * Add the supplied filters to the entity table query
     */
    public function query(Query $query): Query
    {
        if ($this->hasFilters()) {
            foreach ($this->filters as $filter) {
                $filter->query($query, $this->data);
            }
        }
        return $query;
    }

    /**
     * Return the Form
     *
     * @return FilterForm
     */
    public function getForm(): FilterForm
    {
        if (!$this->form instanceof FilterForm) {
            $form = new FilterForm();
            foreach ($this->filters as $filter) {
                $form->addFieldset($filter->getFieldset());
                $form->addValidators($filter->getValidators());
            }
            $this->form = $form;
        }
        return $this->form;
    }

    public function getData(): array
    {
        return $this->data;
    }
}
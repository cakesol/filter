<?php

namespace Cakesol\Filter\Filters;

use Cakesol\Filter\Model\Fieldset;
use Cakesol\Filter\Model\Field;
use Cakesol\Filter\Model\Validator;
use Cake\ORM\Query;

abstract class AbstractFilter implements FilterInterface
{
    /**
     * @var Fields[] $fields
     */
    protected $fields;

    public function __construct()
    {

    }

    /**
     * @param Query $query
     * @param array $data
     * @return Query
     */
    public function query(Query $query, array $data): Query
    {
        $fields = $this->getFields();
        foreach ($fields as $field) {
            $this->queryField($query, $data, $field);
        }
        return $query;
    }

    /**
     * @param Query $query
     * @param array $data
     * @param Field $field
     * @return Query
     */
    private function queryField(Query $query, array $data, Field $field): Query
    {
        if (!empty($data[$field->getName()])) {
            $match = $data[$field->getName()];
            return $query->where(function ($exp, $query) use ($match, $field) {
                return $exp->like($field->getName(), "%$match%");
            });
        }
        return $query;
    }
}
<?php

namespace Cakesol\Filter\Filters;

use Cakesol\Filter\Model\Fieldset;
use Cakesol\Filter\Model\Field;
use Cakesol\Filter\Model\Validator;

class DistanceFilter extends AbstractFilter
{
    /**
     * @return Fieldset
     */
    public function getFieldset()
    {
        return new Fieldset('Email', [
            new Field('email', ['type' => 'string']),
            new Field('search', ['type' => 'string']),
        ]);
    }

    /**
     * @return array
     */
    public function getValidators()
    {
        return [
            new Validator('email', 'format', [
                'rule' => 'email',
                'message' => 'A valid email address is required',
            ]),
            new Validator('search', 'length', [
                'rule' => ['minLength', 10],
                'message' => 'A name is required',
            ]),
        ];
    }
}
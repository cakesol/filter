<?php

namespace Cakesol\Filter\Filters;

use Cake\ORM\Query;
use Cakesol\Filter\Model\Fieldset;
use Cakesol\Filter\Model\Field;
use Cakesol\Filter\Model\Validator;

class EmailFilter extends AbstractFilter
{
    /**
     * @return Fieldset
     */
    public function getFieldset(): Fieldset
    {
        return new Fieldset('Email', $this->getFields());
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        if (!is_array($this->fields)) {
            $this->fields = [
                new Field('email', ['type' => 'string']),
            ];
        }

        return $this->fields;
    }

    /**
     * @return array
     */
    public function getValidators(): array
    {
        return [
            new Validator('email', 'format', [
                'rule' => 'email',
                'message' => 'A valid email address is required',
            ])
        ];
    }
}
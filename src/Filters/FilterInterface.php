<?php

namespace Cakesol\Filter\Filters;

use Cake\Orm\Query;
use Cakesol\Filter\Model\Fieldset;

interface FilterInterface
{
    public function __construct();
    public function query(Query $query, array $data): Query;
    public function getFields(): array;
    public function getFieldset(): Fieldset;
    public function getValidators(): array;
}
<?php

namespace Cakesol\Filter\Filters;

use Cakesol\Filter\Model\Fieldset;
use Cakesol\Filter\Model\Field;
use Cakesol\Filter\Model\Validator;

class StringFilter extends AbstractFilter
{
    /**
     * @return Fieldset
     */
    public function getFieldset(): Fieldset
    {
        return new Fieldset('Zoeken', $this->getFields());
    }

    /**
     * @return Fields[]
     */
    public function getFields(): array
    {
        if (!is_array($this->fields)) {
            $this->fields = [
                new Field('search', ['type' => 'string']),
            ];
        }
        return $this->fields;
    }

    /**
     * Return an array with validators for the fields
     *
     * @return Validator[]
     */
    public function getValidators(): array
    {
        return [
            new Validator('search', 'length', [
                'rule'    => ['minLength', 10],
                'message' => 'A name is required',
            ]),
        ];
    }
}
<?php

namespace Cakesol\Filter\Form;

use Cakesol\Filter\Model\Field;
use Cakesol\Filter\Model\Fieldset;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class FilterForm extends Form
{
    private $fieldsets = [];
    private $fields = [];
    public $validationRules = [];

    protected function _buildSchema(Schema $schema)
    {
        foreach ($this->fields as $field) {
            $schema->addField($field->getName(), $field->getAttributes());
        }
        return $schema;
    }

    protected function _buildValidator(Validator $validator)
    {
        foreach ($this->validationRules as $validate) {
            if ($validator instanceof Validator) {
                $validator->add($validate->getField(), $validate->getName(), $validate->getRules());
            }
        }

        return $validator;
    }

    public function getFields(): array
    {
        return $this->fields;
    }

    public function getFieldsets(): array
    {
        return $this->fieldsets;
    }

    /**
     * @param array $field
     * @return FilterForm
     */
    public function addField(Field $field): self
    {
        $this->fields[] = $field;
        return $this;
    }

    /**
     * @param array $field
     * @return FilterForm
     */
    public function addFieldset(Fieldset $fieldset): self
    {
        foreach ($fieldset->getFields() as $field) {
            $this->addField($field);
        }
        $this->fieldsets[] = $fieldset;

        return $this;
    }

    /**
     * @param Validator[] $field
     * @return FilterForm
     */
    public function addValidators(array $validator): self
    {
        $this->validationRules = array_merge($this->validationRules, $validator);
        return $this;
    }

    protected function _execute(array $data)
    {
        // Send an email.
        return true;
    }
}
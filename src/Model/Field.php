<?php

namespace Cakesol\Filter\Model;

class Field
{
    /** @var string */
    private $name;
    /** @var array  */
    private $attributes;

    /**
     * Fieldset constructor.
     * @param string $name
     * @param array $attributes
     */
    public function __construct(string $name, array $attributes = [])
    {
        $this->name = $name;
        $this->attributes = $attributes;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }
}
<?php

namespace Cakesol\Filter\Model;

use Cakesol\Filter\Model\Field;

class Fieldset
{
    /** @var string */
    private $title;
    /** @var Field[]  */
    private $fields;

    /**
     * Fieldset constructor.
     * @param string $title
     * @param Field[] $fields
     */
    public function __construct(string $title, array $fields = [])
    {
        $this->title = $title;
        $this->fields = $fields;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return Fields[]
     */
    public function getFields(): array
    {
        return $this->fields;
    }
}
<?php

namespace Cakesol\Filter\Model;

class Validator
{
    /** @var string */
    private $field;
    /** @var string */
    private $name;
    /** @var array  */
    private $rules;

    /**
     * Validator constructor.
     * @param string $field
     * @param string $name
     * @param array $attributes
     */
    public function __construct(string $field, string $name, array $rules = [])
    {
        $this->field = $field;
        $this->name = $name;
        $this->rules = $rules;
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getRules(): array
    {
        return $this->rules;
    }
}
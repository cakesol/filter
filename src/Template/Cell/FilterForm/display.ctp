<?php
if ($filter->hasFilters()) :
    echo $this->Form->create($filter->getForm());
    foreach ($filter->getForm()->getFieldsets() as $fieldset) :
        echo $this->element('Form/fieldset', ['fieldset' => $fieldset]);
    endforeach;
    echo $this->Form->button('Submit');
    echo $this->Form->end();
endif;
?>
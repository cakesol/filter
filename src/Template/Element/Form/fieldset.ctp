<fieldset>
    <legend><?= $fieldset->getTitle()?>:</legend>
        <?php
        foreach ($fieldset->getFields() as $field) :
            echo $this->Form->control($field->getName(), $field->getAttributes());
        endforeach;
        ?>
</fieldset>
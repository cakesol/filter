<?php

namespace Cakesol\Filter\View\Cell;

use Cake\View\Cell;

class FilterFormCell extends Cell
{

    public function display(\Cakesol\Filter\FilterCollection $filter)
    {
        $this->set(compact('filter'));
    }

}